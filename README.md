# Backend CRUD API REST

Ejemplo de WS REST con NodeJS que proporciona un API CRUD para gestionar una DB MongoDB.

## Comenzando 🚀

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

### Pre-requisitos 📋

Primero debemos instalar el gestor de paquetes de Node **npm** el cual nos permite mediante la utilidad **n** instalar y mantener las versiones de Node JS.
```
$ sudo apt install npm
$ sudo npm i -g n
$ sudo n stable
```
El siguiente paso consiste en instalar nuestra base de datos no estructurada.
```
$ sudo apt install -y mongodb
```

### Instalación 🔧
Comenzaremos creando una carpeta llamada **node** para ello abriremos el terminal y pondremos los siguientes comandos.
```
$ cd
$ mkdir node
$ cd node
```
Para clonar el Bitbucket usaremos los siguientes comandos.
```
$ git clone http://srg64@bitbucket.org/srg64/api-rest api-rest
$ cd api-rest
```
Con esto, clonaremos el contenido en una carpeta llamada api-rest, y después nos posicionaremos en esta.

Una vez hayamos clonado el directorio, instalaremos las dependencias del proyecto.
```
$ sudo npm i
```
Para comprobar el estado de los datos del sistema, con el siguiente comando desplegamos la API.
```
$ npm start
```

## Construido con 🛠️
* [Visual Studio Code](https://code.visualstudio.com/) - Editor de texto utilizado para la creación de la API.
* [Postman](https://www.postman.com/) - Aplicación para hacer pruebas de la API.
* [Node JS](https://nodejs.org/es/) - Entorno de ejecución para JavaScript.
* [Nodemon](https://www.npmjs.com/package/nodemon) - Utilidad de interfaz de linea de comandos, nos permite ver los cambios realizados en la API.
* [MongoDB](https://www.mongodb.com/es) - Sistema de Base de Datos  NoSQL utilizado en el proyecto.

## Repositorio 🖇️
Enlace al [repositorio](https://bitbucket.org/srg64/api-rest/src/master/) del proyecto.

## Versionado 📌
Para acceder a las diferentes versiones del proyecto:

* [v1.0.25](https://bitbucket.org/srg64/api-rest/commits/tag/v1.0.25) 

* [v2.0.0](https://bitbucket.org/srg64/api-rest/commits/tag/v2.0.0)

* [v3.0.0](https://bitbucket.org/srg64/api-rest/commits/tag/v3.0.0) 

Actualmente nos encontramos en la v3.0.0.

## Autores ✒️
* **Sara Ródenas González** - *Trabajo Inicial y Documentación* 

## Expresiones de Gratitud 🎁

* A mi amigo Ignacio por ayudarme con el Readme.